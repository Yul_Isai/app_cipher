package com.yivg.app21.Crypto;

import static java.security.MessageDigest.getInstance;

import com.yivg.app21.Utils.Alert;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import javax.crypto.SecretKey;

public class SymmetricCrypt {
    private SecretKey secretkey;
    private String algorithm;
    private String ciphermode;
    private String padding;

    public SymmetricCrypt(String algorithm, String ciphermode, String padding) {
        this.algorithm = algorithm;
        this.ciphermode = ciphermode;
        this.padding = padding;
    }

    public void setKey(String password) throws UnsupportedEncodingException {
        try {
            byte[] passkey = password.getBytes(StandardCharsets.UTF_8);
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            passkey = md.digest(passkey);
            passkey = Arrays.copyOf(passkey, 16);
            secretkey = new SecretKeySpec(passkey, algorithm);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            Alert.showSnack("Error: invalid algorithm");
        }
        Alert.showToast("Key saved");
    }

    public String encrypt(String plainText) {
        String cipherScheme = algorithm + "/" + ciphermode + "/" + padding;
        try {
            Cipher cipher = Cipher.getInstance(cipherScheme);
            cipher.init(Cipher.ENCRYPT_MODE, secretkey);
            byte[] c = cipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8));
            String cipherText = Base64.getEncoder().encodeToString(c);
            Alert.showToast("Text Encrypted");
            return cipherText;
        } catch (NoSuchAlgorithmException e) {
            Alert.showSnack("Error: Invalid encryption algorithm");
        } catch (NoSuchPaddingException e) {
            Alert.showSnack("Error: Invalid padding algorithm");
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            Alert.showSnack("Error: Invalid key format");
            e.printStackTrace();
        } catch (BadPaddingException e) {
            Alert.showSnack("Error: Bad padding");
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            Alert.showSnack("Error: Illegal block size");
            e.printStackTrace();
        }
        return null;
    }

    public String decrypt(String cipherText) {
        String cipherScheme = algorithm + "/" + ciphermode + "/" + padding;
        try {
            Cipher cipher = Cipher.getInstance(cipherScheme);
            cipher.init(Cipher.DECRYPT_MODE, secretkey);
            byte[] p = Base64.getDecoder().decode(cipherText);
            String plainText = new String(cipher.doFinal(p));
            Alert.showToast("Text Decrypted");
            return plainText;
        } catch (NoSuchAlgorithmException e) {
            Alert.showSnack("Error: Invalid encryption algorithm");
        } catch (NoSuchPaddingException e) {
            Alert.showSnack("Error: Invalid padding algorithm");
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            Alert.showSnack("Error: Invalid key format");
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            Alert.showSnack("Error: Illegal block size");
            e.printStackTrace();
        } catch (BadPaddingException e) {
            Alert.showSnack("Error: Bad padding");
            e.printStackTrace();
        }
        return null;
    }


}
