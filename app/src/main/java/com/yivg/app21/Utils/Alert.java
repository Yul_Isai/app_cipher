package com.yivg.app21.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public abstract class Alert {

    @SuppressLint("StaticFieldLeak")
    private static Context context;
    private static View parent;


    public static void setContext(Context context, View parent) {
        Alert.context = context;
        Alert.parent = parent;
    }

    public static  void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void showSnack(String message) {
        Snackbar.make(parent, message, Snackbar.LENGTH_SHORT).show();
    }
}
