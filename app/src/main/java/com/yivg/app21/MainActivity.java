package com.yivg.app21;

import android.os.Bundle;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android.os.Environment;
import android.view.View;

import androidx.navigation.ui.AppBarConfiguration;

import com.google.android.material.textfield.TextInputEditText;
import com.yivg.app21.Crypto.SymmetricCrypt;
import com.yivg.app21.Utils.Alert;
import com.yivg.app21.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;

import javax.crypto.Cipher;


import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;
    private EditText edtPass;
    private EditText edtMessage;
    private RadioButton rbtnE;
    private RadioButton rbtnD;

    private SymmetricCrypt cipher;
    private String filename = "EncryptedFile.txt";
    private String filePath = "crypto";
    private File myexternalFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        edtPass = (EditText) findViewById(R.id.outTxt1);
        edtMessage = (EditText) findViewById(R.id.outTxt2);
        rbtnE = (RadioButton) findViewById(R.id.radio_button_1);
        rbtnD = (RadioButton) findViewById(R.id.radio_button_2);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(this);

        cipher = new SymmetricCrypt("AES", "ECB", "PKCS5Padding");
    }


    private void encryptMessage() {
        String pass = edtPass.getText().toString();
        try {
            cipher.setKey(pass);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String txt = cipher.encrypt(edtMessage.getText().toString());
        edtPass.setText("");
        edtMessage.setText(txt);
        edtMessage.setTextColor(getResources().getColor(R.color.pink_500, null));

        if (isExternalStorageReady()) {
            myexternalFile = new File(getExternalFilesDir(filePath), filename);
            write();
        }
    }

    private boolean isExternalStorageReady() {
        String externalStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(externalStorageState) && !Environment.MEDIA_MOUNTED_READ_ONLY.equals(externalStorageState)) {
            return true;
        }
        return false;
    }

    private void write() {
        try {
            FileOutputStream fo = new FileOutputStream(myexternalFile);
            fo.write(edtMessage.getText().toString().getBytes());
            fo.close();
        } catch (IOException e) {
            Alert.showSnack("Error writing file");
        }
        Alert.showToast("File saved");
    }


    private void decryptMessage() {
        String pass = edtPass.getText().toString();
        try {
            cipher.setKey(pass);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String cipherText = "";

        if (isExternalStorageReady()) {
            myexternalFile = new File(getExternalFilesDir(filePath), filename);
            cipherText = read();
        }

        String text = cipher.decrypt(cipherText);

        edtPass.setText("");
        edtMessage.setText(text);
        edtMessage.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
        edtMessage.setTextColor(getResources().getColor(R.color.pink_500, null));
    }


    public String read() {
        String mydata = "";
        try {
            FileInputStream fi = new FileInputStream(myexternalFile);
            DataInputStream di = new DataInputStream(fi);
            BufferedReader br = new BufferedReader(new InputStreamReader(di));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                mydata = mydata + strLine;
            }
            di.close();
        } catch (IOException e) {
            Alert.showSnack("Error reading file");
            e.printStackTrace();
        }
        Alert.showToast("File Read");
        return  mydata;
    }


    public void onClick(View v) {
        Alert.setContext(getApplicationContext(), v);
        if (rbtnE.isChecked()) {
            encryptMessage();
        } else if (rbtnD.isChecked()) {
            decryptMessage();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_exit) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        return false;
    }


}